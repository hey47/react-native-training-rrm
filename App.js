import { StyleSheet, Text, View } from 'react-native';
import InitialScreen from './src/navigation/navigate'

import LoginProvider from './src/context/LoginProvider';

export default function App() {
  return (
    <LoginProvider>
      <InitialScreen />
    </LoginProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
