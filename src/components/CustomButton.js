import { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { colors } from '../../styles/colors';

const CustomButtom = ({ title, backgroundColor, color, onPress, width }) => {

    return (
        <TouchableOpacity style={[{backgroundColor}, styles.button]} onPress={onPress}>
            <Text style={[{color}, styles.text]}>{title}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    button: {
        width: '90%',
        borderWidth: 1,
        borderColor: colors.MAIN_COLOR, 
        borderRadius: 10,
        marginVertical: 5
    },
    text: {
        fontSize: 16,
        textAlign: 'center',
        padding: 10,
        fontWeight: '600',
    },
})

export default CustomButtom;