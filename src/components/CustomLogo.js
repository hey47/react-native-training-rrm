import { StyleSheet, Text, } from 'react-native';
import { colors } from '../../styles/colors';

const CustomLogo = () => {
    return <Text style={styles.heading}>Charity</Text>
}

const styles = StyleSheet.create({
    heading: { 
        fontSize: 50, 
        fontWeight: 'bold', 
        color: colors.MAIN_COLOR,
        lineHeight: 51,
        marginBottom: 5
    },
})

export default CustomLogo;