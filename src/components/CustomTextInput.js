import { StyleSheet, View, TextInput, Text } from 'react-native'
import { Ionicons } from '@expo/vector-icons'
import { colors } from '../../styles/colors'

const CustomTextInput = ({
        label, error, value, onChangeText, placeholder, 
        iconName, style, keyboardType, onPressIcon, onBlur,
        secureTextEntry, textInputColor
    }) => {
    return (
        <View style={style ? style : styles.container}>
            <View style={styles.errorLabelView}>
                {label ? (<Text style={[{ color: colors.GREY_COLOR}, styles.labelStyle]}>{label}</Text>) : null}
                {error ? (<Text style={[{ color: colors.MAIN_COLOR}, label ? styles.errorStyleLabel : styles.errorStyle]}>{error}</Text>) : null}
            </View>
            <View style={[styles.inputView, {borderColor: textInputColor}]}>
                <TextInput
                    style={[styles.input, {color: textInputColor}]}
                    value={value}
                    placeholder={placeholder}
                    secureTextEntry={secureTextEntry}
                    onChangeText={onChangeText}
                    keyboardType={keyboardType}
                    autoCapitalize='none'
                    onBlur={onBlur} />
                {iconName && (
                    <Ionicons 
                        style={styles.icons} 
                        name={iconName} 
                        size={20} 
                        color={textInputColor} 
                        onPress={onPressIcon}/>
                )
                }

            </View>
        </View>
    )
}

export default CustomTextInput

const styles = StyleSheet.create({
    container: {
        width: '90%',
    },
    icons: {
        textAlign: 'right'
    },
    inputView: {
        width: '100%',
        borderWidth: 1,
        marginBottom: 10,
        padding: 10,
        borderColor: colors.MAIN_COLOR,
        borderRadius: 10,
        flexDirection: 'row'
    },
    input: {
        width: '100%',
        fontSize: 16,
        color: colors.MAIN_COLOR,
    },
    errorLabelView: {
        width: '100%',
        flexDirection: 'row'
    },
    errorStyle: {
        fontSize: 16,
        marginBottom:4, 
        paddingStart:4,
        fontWeight: '400',
    },
    errorStyleLabel:{
        fontSize: 16,
        marginBottom:4, 
        paddingStart:4,
        fontWeight: '400',
    },
    labelStyle: {
        fontSize: 16,
        marginBottom:4, 
        fontWeight: '600',
    }
})