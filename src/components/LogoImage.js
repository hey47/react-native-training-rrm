import { StyleSheet, Image } from 'react-native';

const LogoImage = () => {
    return (
        <Image 
            style={styles.image}
            source={require('../../assets/logo.jpg')}/>
    )
}

const styles = StyleSheet.create({
    image: {
        width: 138,
        height: 138,
        marginBottom: 20
    }
})

export default LogoImage;