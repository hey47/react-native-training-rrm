import { useState } from 'react'
import { Text, View, StyleSheet, } from 'react-native'
import { Formik } from 'formik'
import Checkbox from 'expo-checkbox';

import { colors } from '../../../styles/colors'
import CustomTextInput from '../CustomTextInput'
import CustomButton from '../CustomButton'
import { validateArea, validateDate, validateEmailPassConfirmPass, validateNames, validatePhoneNumber } from '../../validators/validator';

export const NameSurnameForm = ( {onNextPressed, onSave} ) => {

    return (
        <>
            <Text style={styles.slogan}>Наша задача - предоставить ясность</Text>
            <Formik
                initialValues={{ firstName: '', lastName: '' }}
                validationSchema={validateNames}
                onSubmit={(values) => {
                    onSave(values)
                    onNextPressed()
                }}
            >
                {({ 
                    values, 
                    errors,
                    touched,
                    isSubmitting,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                }) => {
                    const { firstName, lastName } = values;

                    return (
                        <View style={styles.container}>
                            <CustomTextInput
                                style={{width: '97%'}}
                                value={firstName}
                                onChangeText={handleChange('firstName')}
                                placeholder='Наталья'
                                label='Ваше имя:'
                                textInputColor={colors.GREY_COLOR}
                                error={touched.firstName && errors.firstName}
                                onBlur={handleBlur('firstName')}
                                />
                            <CustomTextInput
                                style={{width: '97%'}}
                                value={lastName}
                                onChangeText={handleChange('lastName')}
                                placeholder='Смакова'
                                label='Ваша фамилия:'
                                textInputColor={colors.GREY_COLOR}
                                error={touched.lastName && errors.lastName}
                                onBlur={handleBlur('lastName')}
                                />
                            <CustomButton
                                title='Продолжить'
                                onPress={handleSubmit}
                                backgroundColor={colors.MAIN_COLOR}
                                color={'#fff'}
                                />
                        </View>
                    )
                }}
            </Formik>
        </>
    )
}

export const DateForm = ({onNextPressed, onSave}) => {
    const [error, setError] = useState('');

    return (
        <Formik
            style={styles.container}
            initialValues={{ day: '', month: '', year: '' }}
            validationSchema={validateDate}
            onSubmit={(values) => {
                const dob = {dob : new Date(values.year + '-' + values.month + '-' + values.day)}
                onSave(dob)
                onNextPressed()
            }}
        >
            {({ 
                values, 
                errors,
                touched,
                isSubmitting,
                handleChange,
                handleBlur,
                handleSubmit,
             }) => {
                const { day, month, year } = values; 
                return (
                    <>
                        <Text style={styles.label}>Дата вашего рождения:</Text>
                        <View style={styles.dateContainer}>
                            <CustomTextInput
                                style={styles.dateInput}
                                value={day}
                                textInputColor={colors.GREY_COLOR}
                                onChangeText={handleChange('day')}
                                placeholder='ДД'
                                keyboardType='numeric'
                                error={touched.day && errors.day}
                                onBlur={handleBlur('day')}
                            />
                            <CustomTextInput
                                style={styles.dateInput}
                                value={month}
                                textInputColor={colors.GREY_COLOR}
                                onChangeText={handleChange('month')}
                                placeholder='ММ'
                                keyboardType='numeric'
                                error={touched.month && errors.month}
                                onBlur={handleBlur('month')}
                            />
                            <CustomTextInput
                                style={styles.dateInput}
                                value={year}
                                textInputColor={colors.GREY_COLOR}
                                onChangeText={handleChange('year')}
                                placeholder='ГГГГ'
                                keyboardType='numeric'
                                error={touched.year && errors.year}
                                onBlur={handleBlur('year')}
                            />
                        </View>
                        <CustomButton
                            title='Продолжить'
                            onPress={handleSubmit}
                            backgroundColor={colors.MAIN_COLOR}
                            color={'#fff'}
                            />
                    </>
                )
             }}
        </Formik>
    )
}

export const AreaForm = ({onNextPressed, onSave}) => {
    return (
        <Formik
            style={styles.container}
            initialValues={{ country: '', city: '' }}
            validationSchema={validateArea}
            onSubmit={(values) => {
                onSave(values)
                onNextPressed()
            }}
        >
            {({ 
                values, 
                errors,
                touched,
                isSubmitting,
                handleChange,
                handleBlur,
                handleSubmit,
            }) => {
                const { country, city } = values
                return (
                    <>
                        <Text style={styles.label}>Выберите страну проживания:</Text>
                        <CustomTextInput
                            style={{width: '97%'}}
                            value={country}
                            onChangeText={handleChange('country')}
                            placeholder='Казахстан'
                            textInputColor={colors.GREY_COLOR}
                            error={touched.country && errors.country}
                            onBlur={handleBlur('country')}
                        />
                        <Text style={styles.label}>Выберите Ваш город:</Text>
                        <CustomTextInput
                            style={{width: '97%'}}
                            value={city}
                            onChangeText={handleChange('city')}
                            placeholder='Нур-Султан'
                            textInputColor={colors.GREY_COLOR}
                            error={touched.city && errors.city}
                            onBlur={handleBlur('city')}
                        />
                        <CustomButton
                            title='Продолжить'
                            onPress={handleSubmit}
                            backgroundColor={colors.MAIN_COLOR}
                            color={'#fff'}
                            />
                    </>
                )
            }}
        </Formik>
    )
}

export const PhoneForm = ({validateStep}) => {
    return (
        <Formik
            style={styles.container}
            initialValues={{ phoneNumber: '' }}
            validationSchema={validatePhoneNumber}
            onSubmit={(values, actions) => {
                validateStep(values, 'phoneNumber', actions)
            }}
        >
            {({ 
                values, 
                errors,
                touched,
                isSubmitting,
                handleChange,
                handleBlur,
                handleSubmit,
             }) => {
                const {phoneNumber} = values
                return (
                    <>
                        <Text style={styles.label}>Контактный номер:</Text>
                        <CustomTextInput
                            style={{width: '97%'}}
                            value={phoneNumber}
                            label='Введите номер телефона:'
                            textInputColor={colors.GREY_COLOR}
                            onChangeText={handleChange('phoneNumber')}
                            placeholder='+7 (777) 654-98-74'
                            error={touched.phoneNumber && errors.phoneNumber}
                            onBlur={handleBlur('phoneNumber')}
                        />
                        <CustomButton
                            title='Продолжить'
                            onPress={handleSubmit}
                            backgroundColor={colors.MAIN_COLOR}
                            color={'#fff'}
                            />
                        <View style={styles.noteView}>
                            <Text style={styles.note}>
                                Мы никому не разглашаем Ваши личные данные
                            </Text>
                            <Text style={styles.note}>
                                и соблюдаем все условия конфиденциальности
                            </Text>
                        </View>
                    </>
                )
             }}
        </Formik>
    )
}

export const EmailPasswordForm = ({validateStep, onFinish}) => {
    const [isChecked, setChecked] = useState(false);
    
    // icon actions consts
    const [showPassword, setShowPassword] = useState(true);

    const changePasswordInput = () => {
        if (showPassword) {
            setShowPassword(false)
        } else 
            setShowPassword(true)
    }

    return (
        <Formik
            style={styles.container}
            initialValues={{ email: '', password: '', confirmPassword: '' }}
            validationSchema={validateEmailPassConfirmPass}
            onSubmit={(values, actions) => {
                
                if(!isChecked) {
                    actions.setFieldError('email', 'Нужно Ваше согласие')
                } else {
                    validateStep(values, 'email', actions)
                }
            }}
        >
            {({ 
                values, 
                errors,
                touched,
                isSubmitting,
                handleChange,
                handleBlur,
                handleSubmit,
             }) => {
                const {email, password, confirmPassword} = values
                return (
                    <>
                        <Text style={styles.label}>Введите данные для авторизации:</Text>
                        <CustomTextInput
                            style={{width: '97%'}}
                            value={email}
                            label='Введите почту:'
                            onChangeText={handleChange('email')}
                            placeholder='info@clarity.com'
                            textInputColor={colors.GREY_COLOR}
                            error={touched.email && errors.email}
                            onBlur={handleBlur('email')}
                        />
                        <CustomTextInput
                            style={{width: '97%'}}
                            value={password}
                            label='Введите пароль:'
                            onChangeText={handleChange('password')}
                            placeholder='Clarity123456'
                            textInputColor={colors.GREY_COLOR}
                            error={touched.password && errors.password}
                            onBlur={handleBlur('password')}
                            secureTextEntry = {showPassword}
                            iconName={showPassword ? "eye-off-sharp" : "eye-sharp"}
                            onPressIcon={changePasswordInput}
                        />
                        <CustomTextInput
                            style={{width: '97%'}}
                            value={confirmPassword}
                            label='Повторите пароль:'
                            onChangeText={handleChange('confirmPassword')}
                            placeholder='Clarity123456'
                            textInputColor={colors.GREY_COLOR}
                            error={touched.confirmPassword && errors.confirmPassword}
                            onBlur={handleBlur('confirmPassword')}
                            secureTextEntry = {showPassword}
                        />
                        <View style={styles.politics}>
                            <Checkbox
                                style={styles.checkbox}
                                value={isChecked}
                                onValueChange={setChecked}
                                color={isChecked ? colors.GREY_COLOR : undefined}
                                errors={errors.confirmPassword}
                                />
                            <View>
                                <Text style={styles.politicsText}>
                                    Ознакомлена с <Text style={{textDecorationLine:"underline"}}>
                                        Пользовательским соглашением
                                        </Text>
                                </Text>
                            </View>
                        </View>
                        <CustomButton
                            title='Продолжить'
                            onPress={handleSubmit}
                            backgroundColor={colors.MAIN_COLOR}
                            color={'#fff'}
                            />
                    </>
                )
            }}
        </Formik>
    )
}

export const CongratsForm = ({navigation}) => {
    return (
        <View style={styles.container}>
            <Text style={styles.congratsText}>Поздравляем!</Text>
            <View style={[styles.noteView, {marginVertical: 15}]}>
                <Text style={styles.congratsSubText}>
                    Теперь Вы можете получить ясные ответы
                </Text>
                <Text style={styles.congratsSubText}>
                    и консультации для вашего женского
                </Text>
                <Text style={styles.congratsSubText}>
                    здоровья от лучших врачей!
                </Text>
            </View>
            <CustomButton
                title='Перейти на главную страницу'
                onPress={() => {navigation.navigate('MainScreen')}}
                backgroundColor={'white'}
                color={colors.MAIN_COLOR}
                />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        alignItems: 'center'
    },
    slogan: {
        color: colors.MAIN_COLOR,
        justifyContent: 'center',
        fontWeight: '600',
        fontSize: 16,
        margin: 10
    },
    labelWrapper: {
        width: '100%',
        alignItems: 'flex-start',
    },
    label: {
        color: colors.MAIN_COLOR,
        fontSize: 16,
        fontWeight: '600',
        marginVertical: 10
    },
    dateContainer: {
        flexDirection: 'row',
        marginTop: 7
    },
    dateInput: {
        flex: 1,
        marginHorizontal: 5
    },
    note: {
        color: colors.GREY_COLOR,
        fontSize: 14,
        lineHeight: 14,
    },
    noteView: {
        textAlign: 'center',
        marginVertical: 5
    },
    congratsText: {
        color: colors.MAIN_COLOR,
        fontSize: 40,
        fontWeight: '700',
        lineHeight: 40,
    },
    congratsSubText: {
        color: colors.MAIN_COLOR,
        fontSize: 16,
        lineHeight: 16,
        fontWeight: '500',
    },
    checkbox: {
        marginRight: 10,
    },
    politics: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 10
    },
    politicsText: {
        fontSize: 12,
        color: colors.GREY_COLOR
    },
})