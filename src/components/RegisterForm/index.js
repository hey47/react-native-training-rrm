import { Text, View, StyleSheet, Button } from 'react-native';
import StepIndicator from 'react-native-step-indicator';
import { colors } from '../../../styles/colors';
import { useState } from 'react';

import client from '../../api/client';
import { NameSurnameForm, DateForm, AreaForm, PhoneForm, EmailPasswordForm, CongratsForm } from './forms'
import CustomLogo from '../CustomLogo';

const siStyles = {
    stepIndicatorSize: 40,
    currentStepIndicatorSize: 40,
    separatorStrokeWidth: 1,
    stepStrokeWidth: 2,
    currentStepStrokeWidth: 0,
    stepStrokeUnFinishedColor: colors.GREY_COLOR,
    stepStrokeFinishedColor: colors.MAIN_COLOR,
    separatorFinishedColor: colors.MAIN_COLOR,
    separatorUnFinishedColor: colors.GREY_COLOR,
    stepIndicatorFinishedColor: colors.MAIN_COLOR,
    stepIndicatorUnFinishedColor: 'white',
    stepIndicatorCurrentColor: colors.MAIN_COLOR,
    stepIndicatorLabelFontSize: 15,
    currentStepIndicatorLabelFontSize: 15,
    stepIndicatorLabelCurrentColor: 'white',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: colors.GREY_COLOR,
}

export default function RegisterForm({navigation}) {
    // page consts and actions
    const [currentPage, setCurrentPage] = useState(0);

    const onNextPressed = () => {
        setCurrentPage(currentPage + 1)
    }

    // userInfo consts and actions
    const [userInfo, setUserInfo] = useState({
        avatar: '',
        firstName: '',
        lastName: '',
        dob: '',
        country: '',
        city: '',
        phoneNumber: '',
        email: '',
        password: '',
        confirmPassword: '',
    });

    const saveUserValue = (value) => {
        setUserInfo({ ...userInfo,  ...value});          
        console.log('Function call', userInfo)
    };   

    // register method call
    const validateStep = async(values, fieldName, formikActions) => {
        console.log(formikActions)
        const res = await client.post('/register', {
            ...userInfo, ...values
        });
        
        if (!res.data.success) {
            formikActions.setFieldError(fieldName, res.data.message)
        } else {
            saveUserValue(values)
            onNextPressed()
        }
    }

    const onFinish = async (values, formikActions) => {
        const signInRes = await client.post('/login', {
        email: values.email,
        password: values.password,
        });
        if (signInRes.data.success) {
        console.log('Logged in success')
        }
    
        formikActions.resetForm();
        formikActions.setSubmitting(false);
    };

    const PAGES = [
        <NameSurnameForm onNextPressed={onNextPressed} onSave={saveUserValue}/>,
        <DateForm onNextPressed={onNextPressed} onSave={saveUserValue}/>,
        <AreaForm onNextPressed={onNextPressed} onSave={saveUserValue}/>,
        <PhoneForm onNextPressed={onNextPressed} validateStep={validateStep}/>,
        <EmailPasswordForm onNextPressed={onNextPressed} validateStep={validateStep} onFinish={onFinish}/>,
        <CongratsForm navigation={navigation}/>
    ];

    return <View style={styles.content}>
        <StepIndicator
            style={styles.stepIndicator}
            currentPosition={currentPage}
            stepCount={6}
            customStyles={siStyles} />
        <View style={styles.formWrapper}>
            {PAGES[currentPage]}
        </View>
        <View style={styles.footer}>
            <Button title='bnt' onPress={()=>{
                console.log('Btn',userInfo)
                }}/>
            <CustomLogo />
            <Text 
                style={styles.link} 
                onPress={()=> {navigation.navigate('LoginScreen')}}
                >У меня есть аккаунт</Text>
        </View>
    </View>

}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        width: '100%',
        top: 10,
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    stepIndicator: {
    },
    formWrapper: {
        alignItems: 'center',
    },
    footer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:20
    },
    link: {
        fontSize: 16,
        color: colors.GREY_COLOR,
    }
});