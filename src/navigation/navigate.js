import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';

import WelcomeScreen from '../screens/WelcomeScreen';
import RegisterScreen from '../screens/RegisterScreen';
import LoginScreen from '../screens/LoginScreen';
import MainScreen from '../screens/MainScreen';

const MyTheme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: 'black',
      background: 'white',
      border: 'white'
    },
  };

const Stack = createStackNavigator();

export default function Navigate() {
    return <NavigationContainer theme={MyTheme}>
        <Stack.Navigator>
            <Stack.Screen 
                name='Welcome'
                component={WelcomeScreen}
                options={
                    {
                        headerShown: false
                        // title: '', 
                        // headerStyle: { backgroundColor: '#eb5d3d', height: 60},
                        // headerTitleStyle: { fontWeight: '400'}
                    }
                }
                />
            <Stack.Screen 
                name='RegisterScreen'
                component={RegisterScreen}
                options={{title: 'Регистрация'}}/>    
            <Stack.Screen 
                name='LoginScreen'
                component={LoginScreen}
                options={{title: 'Авторизация'}}/>    
            <Stack.Screen 
                name='MainScreen'
                component={MainScreen}
                options={
                    {
                        headerShown: false
                    }
                }
                />                      
        </Stack.Navigator>
    </NavigationContainer>
}
