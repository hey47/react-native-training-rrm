import { Text, SafeAreaView, StyleSheet, View} from 'react-native'
import { Formik } from 'formik';
import { useState } from 'react';

import { colors } from '../../styles/colors';
import CustomButtom from '../components/CustomButton';
import LogoImage from '../components/LogoImage';
import client from '../api/client';
import { useLogin } from '../context/LoginProvider';
import CustomTextInput from '../components/CustomTextInput';
import { validateEmailPass } from '../validators/validator';

export default function LoginScreen({ navigation }) {
    // icon actions consts
    const [showPassword, setShowPassword] = useState(true);

    const changePasswordInput = () => {
        if (showPassword) {
            setShowPassword(false)
        } else 
            setShowPassword(true)
    }

    // login consts
    const { setIsLoggedIn, setProfile } = useLogin();
    const userInfo = { email: '', password: ''};

    const submitForm = async (values, formikActions) => {
        // console.log(formikActions);
        try {
            const res = await client.post('/login', { ...values });

            if (res.data.success) {
                setProfile(res.data.user);
                setIsLoggedIn(true);

                console.log(res.data)
                
                formikActions.resetForm();
                formikActions.setSubmitting(false);
            }

            formikActions.setFieldError('email', res.data.message)
            } catch (error) {
                formikActions.setErrors(error)
        }
    };

    return <SafeAreaView style={styles.container}>
        <LogoImage/> 
        <Formik initialValues={userInfo} 
                validationSchema={validateEmailPass}
                onSubmit={submitForm}>
            {({
                values,
                errors,
                touched,
                isSubmitting,
                handleChange,
                handleBlur,
                handleSubmit,
            }) => {
                const { email, password } = values;

                return (
                    <>
                        <CustomTextInput 
                            textInputColor={colors.GREY_COLOR}
                            value={email}
                            placeholder='Почта email'
                            onChangeText={handleChange('email')}
                            error={touched.email && errors.email}
                            onBlur={handleBlur('email')}
                            iconName={'close-circle'}
                            // onPressIcon={changePasswordInput}
                            />
                        <CustomTextInput 
                            textInputColor={colors.GREY_COLOR}
                            value={password}
                            placeholder='Пароль'
                            secureTextEntry = {showPassword}
                            onChangeText={handleChange('password')}
                            error={touched.password && errors.password}
                            onBlur={handleBlur('password')}
                            iconName={showPassword ? "eye-off-sharp" : "eye-sharp"}
                            onPressIcon={changePasswordInput}
                            />
                        <CustomButtom 
                            title='Войти' 
                            color={'white'} 
                            submitting={isSubmitting}
                            backgroundColor={colors.MAIN_COLOR}
                            onPress={handleSubmit} 
                            /> 
                    </>
                )
            }}
        </Formik>
        <View style={styles.registerView}>
            <Text style={{color: colors.GREY_COLOR}}>У вас еще нет учетной записи?</Text>
            <Text style={styles.link} onPress={() => {navigation.navigate('RegisterScreen')}}>Зарегистрируйтесь</Text>
        </View>        
        <Text style={styles.forgotPsdBtn}>Забыли пароль?</Text>
        <Text style={styles.politicsBtn}>Политика конфиденциальности</Text>  
    </SafeAreaView>
}

// LoginScreen.navigationOptions = {
//     headerTitle: 'Авторизация'
// }

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    center: {
        width: '90%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    icons:{
        textAlign: 'right'
    },
    inputView: {
        width: '90%',
        borderWidth: 1, 
        marginBottom: 10, 
        padding: 10,
        borderColor: colors.GREY_COLOR, 
        borderRadius: 10,
        flexDirection: 'row'
    },
    selectedInputView: {
        width: '90%',
        borderWidth: 1, 
        marginBottom: 10, 
        padding: 10,
        borderColor: colors.GREY_COLOR, 
        borderRadius: 10,
        flexDirection: 'row'
    },
    input: {
        width: '90%',
        color: colors.MAIN_COLOR,
        fontSize: 16
    },
    registerView: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10
    },
    forgotPsdBtn: {
        color: colors.MAIN_COLOR,
        textDecorationLine:"underline",
        textAlign: 'center',
        position: 'absolute',
        bottom: 0,
        marginBottom: 45,
    },
    politicsBtn: {
        color: colors.GREY_COLOR,
        textDecorationLine:"underline",
        textAlign: 'center',
        position: 'absolute',
        bottom: 0,
        marginBottom: 20,
    },
    link: {
        color: colors.MAIN_COLOR,
        textDecorationLine:"underline",
        textAlign: 'center'
    }
})