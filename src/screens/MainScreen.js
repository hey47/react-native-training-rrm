import {Text, View, Button, StyleSheet} from 'react-native'

export default function MainScreen ({navigation}) {
    const toLogin = () => {
        navigation.navigate('Login')
    }

    return <View style={styles.center}>
        <Text style={{fontSize: 35}}>Main page</Text>
    </View>
}

MainScreen.navigationOptions = {
    headerTitle: 'Главная страница'
}

const styles = StyleSheet.create({
    center: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})