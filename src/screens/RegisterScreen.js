import { StyleSheet, Text, SafeAreaView, View } from 'react-native';
import RegisterForm from '../components/RegisterForm';

export default function RegisterScreen({ navigation }) {
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.stepper}>
                <RegisterForm navigation={navigation}/>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    stepper: {
        flex: 1,
        marginHorizontal: 15,
    },
})