import { StyleSheet, Text, SafeAreaView, TouchableOpacity } from 'react-native';
import { useState } from 'react';
import { colors } from '../../styles/colors';
import LogoImage from '../components/LogoImage';
import CustomButton from '../components/CustomButton';

export default function WelcomeScreen({ navigation }) {
    return (
        <SafeAreaView style={styles.container}>
            <LogoImage />
            <Text style={styles.topHeading}>Добро пожаловать в</Text>
            <Text style={styles.heading}>Charity</Text>
            <Text style={styles.subHeading}>МЕЖДУНАРОДНЫЙ</Text>
            <Text style={styles.subHeading2}>ОНЛАЙН КОНСИЛИУМ ВРАЧЕЙ</Text>
            
            <CustomButton 
                title={'Войти'} 
                color={colors.MAIN_COLOR} 
                backgroundColor={'white'}
                onPress={() => navigation.navigate('LoginScreen')}
                />

            <CustomButton 
                title={'Создать аккаунт'} 
                color={'white'} 
                backgroundColor={colors.MAIN_COLOR}
                onPress={() => navigation.navigate('RegisterScreen')}
                />
            <Text style={styles.footerText}>для Женского Здоровья</Text>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    topHeading: {
        fontSize: 16,
        color: colors.MAIN_COLOR,
        textAlign: 'center',
        lineHeight: 16,
        marginBottom: 5
    },
    heading: { 
        fontSize: 50, 
        fontWeight: 'bold', 
        color: colors.MAIN_COLOR,
        lineHeight: 51,
        marginBottom: 5
    },
    subHeading: {
        fontSize: 19,
        color: colors.MAIN_COLOR,
        textAlign: 'center',
        lineHeight: 24,
        marginTop: 5
    },
    subHeading2: {
        fontSize: 19,
        color: colors.MAIN_COLOR,
        textAlign: 'center',
        lineHeight: 24,
        marginBottom: 35
    },
    footerText: {
        flex: 1,
        fontSize: 16,
        color: colors.MAIN_COLOR,
        textAlign: 'center',
        position: 'absolute',
        bottom: 0,
        marginBottom: 20
    }
})