import * as Yup from 'yup';

export const validateNames = Yup.object({
    firstName: Yup.string()
        .trim()
        .required('Поле обязательное'),
    lastName: Yup.string()
        .trim()
        .required('Поле обязательное'),
})
export const validateDate = Yup.object({
    day: Yup.string()
        .trim()
        .required('день'),  
    month: Yup.string()
        .trim()
        .required('месяц'),
    year: Yup.string()
        .trim()
        .required('год'),
})
export const validateArea = Yup.object({
    country: Yup.string()
        .trim()
        .required('Поле обязательное'),
    city: Yup.string()
        .trim()
        .required('Поле обязательное'),    
})
export const validatePhoneNumber = Yup.object({
    phoneNumber: Yup.string()
        .trim()
        .required('Поле пустое')
})
export const validateEmailPassConfirmPass = Yup.object({
    email: Yup.string()
        .email('Неверный формат почты')
        .required('Поле обязательное'),
    password: Yup.string()
        .trim()
        .min(8, 'Короткий пароль')
        .required('Поле обязательное'),
    confirmPassword: Yup.string().equals(
        [Yup.ref('password'), null],
        'Пароли не совпадают'
        ).required('Поле обязательное'),
})
export const validateEmailPass = Yup.object({
    email: Yup.string()
        .email('Неверный формат почты')
        .required('Поле обязательно для заполнения'),
    password: Yup.string()
        .trim()
        .min(8, 'Слишком короткий пароль')
        .required('Поле обязательно для заполнения'),
});